.. apim documentation master file, created by
   sphinx-quickstart on Wed Jul 22 10:03:03 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to apim's documentation!
================================

Contents:


.. toctree::
   :maxdepth: 3
   sources/whatIsanapi/main
   sources/whatIsanapimanager/main
   sources/gettingStarted/main
   sources/environmentMapping/main
   sources/apiLifecycle/main
   sources/howTosecureanapi/main
   sources/howTomonitoranapi/main
   sources/troubleshooting
   sources/apiManagementserviceoffer/main
   sources/related_applications
   sources/faq
   sources/glossary
   sources/appendix/main
   sources/contacts



Indices and tables
==================


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
