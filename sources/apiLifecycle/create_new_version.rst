
##################################
Create a new version
##################################

Complete the below steps to create a new version of your API on the API-Manager.

.. note:: Follow this guide if you want to create the same version of your API but for an another **non-production environment**. (example: DEV, INT, UAT, etc...)

URLs access:


   - Open APIs: https://external-api.publishers.integration.ca.cib/publisher
   - Private APIs: https://internal-api.publishers.integration.ca.cib/publisher

From the API-Manager store web portal:

- Choose your API from the list
- Click on "CREATE NEW VERSION" in the top navigation bar

   - Fill in the "New Version" field with the **version of your API** and the **envrionment** 
   
      - Example: v2-INT become v3-INT

   - Your new version of API is created, you will be redirect to the API list
   - Select the newly created API 
   - In the "Lifecycle" section, "Publish" it




