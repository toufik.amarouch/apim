###############################
Life cycle management standards
###############################


Below an illustration on how are managed the resources of an API depending on its state (Case: Max. 2 major versions alive in production)

.. figure::  api_lifecycle.png
   :align:   center


**Step 1:**

- Following a **major evolution** of a resource, the API provider plans to release a new **major version N+1 of the API**:

   - All consumers of the current version N **have to be notified** that a new major N+1 is coming soon
   - Only the new major version N+1 of the API contains the new major version of the resource
   - Once released, the new version N+1 of the API should contain all resources of the past version N (Except deprecated resources)

**Step 2:**

- The **new version N+1** is released:

   - The previous version N is automatically deprecated
   - All consumers are invited to upgrade to the new current major version N+1
   - New consumers can NOT consume the deprecated version N
   - Any new corrective / minor evolution of a resource is integrated only in the new current version N+1 (Deprecated version N is frozen)

**Step 3:**

- A new API **version N+2** is released:

   - See rules defined in step 2
   - The old deprecated version N is retired

The stages in the API-Manager:

- **CREATED**: API is visible in the API-Manager store web portal, but applications can not subscribe to it yet
- **PUBLISHED**: The API is visible in the API-Manager store web portaland available for subscription
- **DEPRECATED**: The API is still deployed in the API-Manager, but it is blocked for subscription
- **RETIRED**: The API is not available from the API-Manager and it is deleted from the API-Manager store web portal




