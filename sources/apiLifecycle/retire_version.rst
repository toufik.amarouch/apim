###########################
Deprecate & retire a version
###########################

From the API-Manager store web portal:

- Choose your API from the list
- Click on "Lifecycle" section 
- Clcik on "Deprecate", and choose "Yes" from the information window
- Your API is deprecated, you can click on "Retire"

.. note:: For API providers: you can delete your API from the API-Manager publisher web portal, only when your API is in the **"RETIRED" stage**
