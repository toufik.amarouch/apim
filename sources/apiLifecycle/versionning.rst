.. _linking-pages-api-lifecycle:

###########
Versionning
###########


This section is dedicated to API providers & consumers. It will give you CACIB good practices for API lifecycle management and how to perform it in the API-Manager.

**************
Best practices
**************

^^^^

Backward compatibility
======================

- Both resource and API components have to be versioned to manage their life cycle
- The version of these components is built on 3 digits for major, minor and corrective evolution:

.. figure::  digit_v2.png
   :align:   center


- A major evolution does not ensure backward compatibility
- A minor or corrective evolution does ensure backward compatibility 

  - Backward compatibiliy: is all the changes that remain compatible with the previous API version 

.. note:: Minor or corrective evolution are totally transparent for consumers

Examples:

- Adding a new ressource, is backward compatible, that is why it is a minor evolution 
- Adding an optional field / parameter, is backward compatible, that is why it is a minor evolution 
- Removing a ressource, is not backward compatible, taht is why it is a major evolution
- Changing the response of the http code, is not backward compatible, taht is why it is a major evolution

======================
Concerns for consumers
======================

From the consumer point of view, the major digit of the API version is explicitly used in the path of the request:


.. figure::  majorversion_path.png
   :align:   center

.. warning:: Any major evolution done on an API will impact all existing consumers


To allow major evolutions happen for an API without interrupting services for all existing consumers, the API provider HAS TO be able to manage at least 2 major versions in production.


Below an illustration on how are managed the resources of an API depending on its state (Case: Max. 2 major versions alive in production)

