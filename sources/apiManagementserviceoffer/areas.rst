Geographical areas covered
##########################

There are two platforms of the API-Manager, with components deployed worldwide (Paris and Singapore), enslaved to centralized governance components

.. figure::  2_platforms.png
   :align:   center

   Macro architecture overview 