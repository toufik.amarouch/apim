Interest of the API-Manager 
##########################

Security
********

^^^^

- Centralize authentication flows and perform a first level access control
- Hot activation and deactivation of consumers access
- Protect systems against injection code attack
- Define and apply controls on data
- Prevent congestion of operational systems by applying traffic control policy at API level (e.g. max 100 requests per second)


IS mastering
************

^^^^

- Have an overview of API exposed and systems that consume them
- Be able to analyse how API are used by consumers
- Manage API lifecycle and ease upgrade to new API versions

Operational efficiency
**********************

^^^^

- Allow users to discover, access to the documentation and test API in self-service
- Allow users to perform an access request to an API, through an automatize process, in self-service
- Generate the API clients sdk automatically in multiple languages and provide it to users through the API store
- Expose API of intranet applications to internet, without requiring additional infrastructures in the DMZ (min 150 k� euro of savings per intranet application)
- Centralise specific measures of PSD2, at a lower cost than implementing it in core systems
- Centralise the implementation of CA Group API norms in order to expose API to CA Group entities

