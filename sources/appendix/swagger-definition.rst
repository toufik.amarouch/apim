.. _linking-pages-appendix-swagger:

##################
Swagger definition
##################

***********
Definition
***********

^^^^

Swagger, also named *OpenAPI Specifications*, is a widely used interface specification format. Swagger specifications are usually defined in JSON or Yaml files. This format allows API providers to describe in normalized way services exposed by an application.

***********
Example
***********

^^^^

.. code-block:: yaml

      Swagger: "2.0"
        info:
          description: "This is a sample server Petstore server.  You can find out more about     Swagger at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).      For this sample, you can use the api key `special-key` to test the authorization     filters."
          version: "1.0.0"
          title: "Swagger Petstore"
          termsOfService: "http://swagger.io/terms/"
          contact:
            email: "apiteam@swagger.io"
          license:
            name: "Apache 2.0"
            url: "http://www.apache.org/licenses/LICENSE-2.0.html"
        host: "petstore.swagger.io"
        basePath: "/v2"
        tags:
        - name: "pet"
          description: "Everything about your Pets"
          externalDocs:
            description: "Find out more"
            url: "http://swagger.io"
        - name: "store"
          description: "Access to Petstore orders"
        - name: "user"
          description: "Operations about user"
          externalDocs:
            description: "Find out more about our store"
            url: "http://swagger.io"
        schemes:
        - "https"
        - "http"
        paths:
          /pet:
            post:
              tags:
              - "pet"
              summary: "Add a new pet to the store"
              description: ""
              operationId: "addPet"
              consumes:
              - "application/json"
              - "application/xml"
              produces:
              - "application/xml"
              - "application/json"
              parameters:
              - in: "body"
                name: "body"
                description: "Pet object that needs to be added to the store"
                required: true
                schema:
                  $ref: "#/definitions/Pet"
              responses:
                405:
                  description: "Invalid input"
              security:
              - petstore_auth:
                - "write:pets"
                - "read:pets"

**********
Useful resources
**********

^^^^

- Official documentation: https://swagger.io/specification/

- Online editor: https://editor.swagger.io/
