.. _linking-pages-env-mapping:

####################
Environment mapping
####################


This section is dedicated to API consumers and API providers

- For consumers, it will help you to identify the right environment of the API-Manager to use for API consumption, depending on the development stage of your consuming application (example: DEV, INT, PREPROD or PROD)

- For providers, it will help you to identify the right environment of the API-Manager on which you will expose your API, depending on the development stage of your providing application (example: DEV, INT, PREPROD or PROD)

.. note:: Eventually, environment mapping differs between open and private APIs. For more info, please refer to :ref:`Open & Private APIs <linking-pages-open-vs-private>` section

Environment mapping for open APIs
*********************************

^^^^

The followning diagram shows the environment mapping for open APIs. It is worth for both consumers and providers. API consumers are described on the left pane, an providers in the right one.

.. figure::  open_api_environment_mapping_vf.png
   :align:   center

   Environment mapping for Open APIs

.. note:: **DEMO environment**: is an environment that is identical to the production one, but with test data. 

Examples for open API providers (green pane)
============================================

   - Let us suppose your CACIB internet application is exposing an open API. As shown on the schema:

      - Your APIs in **DEV, INT, UAT** stages will be expose through the **INTEGRATION API-Manager** 
      - Your APIs in **PREPROD** will be exposed through the **PREPRODUCTION API-Manager**
      - Your APIs in **PROD** will be exposes through the **PRODUCTION API-Manager**

Examples for open API consumers (blue pane)
============================================

   - Let us suppose your CACIB Internet application (in-house developement) wants to consume an **Open API**. (*Example: DIANA*). As shown on the schema:

     
      - Your applications in **DEV, INT, UAT** stages will consume through the **INTEGRATION API-Manager** 
      - Your applications in **PREPROD** will consume through the **PREPRODUCTION API-Manager*
      - Your applications in **PROD** will consume through the **PRODUCTION API-Manager*

   - Let us suppose your CACIB Internet application (Cloud Saas) wants to consume an **Open API**. (*Example: Salesforce SaaS application*). As shown on the schema:

      - Your applications in **DEV, INT, UAT and PREPROD** stages will consume through the **PREPRODUCTION API-Manager** 
      - Your applications in **PROD** will consume through the **PRODUCTION API-Manager**

   - Let us suppose your External application wants to consume an **Open API**. (*Example: LVMH mobile application*)

      - Your applications in **DEV, INT, UAT and PREPROD** stages will consume through the **PRODUCTION API-Manager Sandbox** 
      - Your applications in **PROD** will consume through the **PRODUCTION API-Manager**

.. note:: Only preproduction & production environment of the API-Manager de PREPROD et PROD are open to the Internet.

Environment mapping for Private APIs
************************************

^^^^

.. figure::  private_api_environment_mapping_vf.png
   :align:   center

   Environment mapping for Open APIs


Examples for private API providers (green pane)
================================================

   - Let us suppose your CACIB internet application is exposing an **private API**. As shown on the schema:

      - Your APIs in **DEV, INT, UAT** stages will be expose through the **INTEGRATION API-Manager** 
      - Your APIs in **PREPROD** will be exposed through the **PREPRODUCTION API-Manager**
      - Your APIs in **PROD** will be exposes through the **PRODUCTION API-Manager**


Examples for priavte API consumers (blue pane)
===============================================

   - Let us suppose your CACIB Intranet application wants to consume an **private API**. (*Example: Optim*). As shown on the schema:
     
      - Your applications in **DEV, INT, UAT** stages will consume through the **INTEGRATION API-Manager** 
      - Your applications in **PREPROD** will consume through the **PREPRODUCTION API-Manager**
      - Your applications in **PROD** will consume through the **PRODUCTION API-Manager**

   - Let us suppose your CA-partner application wants to consume an **private API**. (*Example: CAPS application*). As shown on the schema:
     
      - Your applications in **DEV, INT, UAT** stages will consume through the **INTEGRATION API-Manager** 
      - Your applications in **PREPROD** will consume through the **PREPRODUCTION API-Manager**
      - Your applications in **PROD** will consume through the **PRODUCTION API-Manager**

   