.. _linking-pages-prod-cons:

##########
Next steps
##########

**************************
Preproduction & Production
**************************

^^^^


- Explore the catalog of APIs: 

   - From the preproduction store web portal:

      - Open APIs: https://external-api.developers.preprod.ca.cib/store
      - Private APIs: https://internal-api.developers.preprod.ca.cib/store

   - From the production store web portal:

      - Open APIs: https://external-api.developers.ca.cib/store
      - Private APIs: https://internal-api.developers.ca.cib/store

.. note:: The rest of the process is the same for preproduction and production environment 

- Contact **IT4IT team** at support_apim@ca-cib.com to create your application in the API-Manager store web portal

- When your application is created, go directly to **"Step 3: Generate authentication means"** and follow the same steps of the :ref:`How to consume an API Quick Start Guide <linking-pages-cons-qsg>`
