########
Overview
########

This section is dedicated to API consumers.


.. figure::  consumer_vf.png
   :align:   center

   API Consumers

Existing published APIs can be browsed through API-Manager store web portal.

In this portal, you will find useful informations susch as Swagger interface, url access, etc.

In this section, we will go through all steps to make your application consume the services exposed as API through the API-Manager. 

****************
Main concepts
****************

^^^^

.. important:: The following information are very important, it will help you understand the consmption process. 

As explained in :ref:`How to secure an API <linking-pages-howto-secure>` section, APIs are secured by relying on the use of 'access_token'. Consumer application will have to provide an 'access_token' during their API call (*An access_token is a temporary access right*)

Basically, your piece of code will have to:

- Generate an access_token on its behalf
- Call the API by passing the 'access_token'
- Renew the access_token whenever it becomes expired

Access token generation is sepecified by OAuth2/OpenID Connect protocols. Its generation requires each application to be registered first and to own a pair of keys named client_id and client_secret.

That is why this section will cover all steps required to consume an API from scratch:

1) Explore the catalog of APIs

2) Register your application

3) Get credentials keys (*These credentials, pair of keys named 'client_id' and 'client_secret', will be used by your application to generate access_tokens*)

4) Subscribe your registered application to one or many APIs

5) Generate an access_token and consume an API

