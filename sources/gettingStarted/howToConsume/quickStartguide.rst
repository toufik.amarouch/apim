.. raw:: html

    <style> .red {color:red} </style>

.. role:: red

.. _linking-pages-cons-qsg:

#################
Quick start guide
#################

Complete the steps described in the rest of this page to consume an API from the API-Manager store.

.. warning:: This is a Quick start guide for consuming an API which backend is from a **non-production environment**. (example: DEV, INT, UAT, etc...)

.. seealso::

  - If you want to consume an API which backend is from the **preproduction environment**, please go to: :ref:`Consuming an API from the preproduction store <linking-pages-prod-cons>`

  - If you want to consume an API which backend is from the **production environment**, please go to: :ref:`Consuming an API from the production store <linking-pages-prod-cons>`


^^^^

Access to the API-Manager **integration** store web portal:

   - For Open APIs: https://external-api.int.ca.cib/store/
   - For Private APIs: https://internal-api.int.ca.cib/store/

.. note:: 
   - Consuming steps will be the same for both Open and Private APIs
   - On the API-Manager integration store web portal , you will find APIs in different development stages (DEV, INT, UAT, QUALIF, etc...)

********************
Step 1 - Exploration
********************

^^^^

From the home page:

- In the "API section", you will have access to all the APIs available
- If you click on an API you will  also have access to its documentation, general information (version, owner, description...)


.. tip:: All the steps and the fields describe below are :red:`mandatory` to consume an API.

**********************************
Step 2 - Register your application
**********************************

^^^^

To register your application:

- Click on "APPLICATIONS" section from the feft navigation bar
- Click on  **"ADD A NEW APPLICATION"**
- Then, fill in the information required:

   - Name of the application

      - *Example: MY-APPLICATION*

   - Description
   - Entity of the application

      - *Allowed values: CACIB or CA-partner*

   - Application Code

      - *Allowed values: Casewise or Arche code*

**************************************
Step 3 - Generate authentication means
**************************************

^^^^

In order to generate access_tokens, credentials keys (client_id and client_secret) have to be generated for your application. The application responsible for providing credentials keys and acces_tokens is UsignOn.

.. note::
   - UsignOn Internet will provide credentials keys for OPEN APIs
   - UsignOn Intranet will provide credentials keys for Private APIs


- As of today:

   - You have to contact **IT4IT team** at support_apim@ca-cib.com to ask for you credentials
   - Click on "Production Keys" section from the application page
   - Click on "Provide Keys" and fill in with the credentials that IT4IT team send you

- Tomorrow:

From the application page:

   - Click on the **"Production Keys"** section
   - Click on **"Generate Keys"**
   - Click on "Show Keys" to get your credentials

*************************************
Step 4 - Subscribe to an API
*************************************

^^^^

Your application has to be **subscribed** to the API before being able to consume it. Note that an application can be suscribed to one or many APIs.


To subscribe your application to an API:

- Click on "API" section from the left navigation bar
- Select an API
- Choose the application to subscribe and the SLA to apply

*************************************
Step 4 - Generate an access_token and consume an API
*************************************

On API-Manager store web portal, from the API page, get the API-Manager Gateway URL.

Use your credentials to generate an access token:

.. code-block:: python

   curl -X POST \
   https://api-gateway-asia.preprod-eqx.asia.cib/connect_asia/v1/token/idp1 \
   -H 'Authorization: Basic <YOUR_CLIENT_ID>:<CLIENT_SECRET>' \
   -H 'Content-Type: application/x-www-form-urlencoded' \
   -d grant_type=client_credentials 

From the response, get your access_token:

For example: 

.. code-block:: json

   {
    "access_token": "<YOUR_ACCESS_TOKEN>",
    "token_type": "bearer",
    "expires_in": 7200
   }

.. note:: 
   - In this simple example, we generated an access_token by performing 'client_credentials' OAuth2 grant. This grant allows to generate access_token on behalf an application *('Application access_token', i.e. access_token is not attached to one individual)*
   - Some APIs can restrict their use to access tokens generated on behalf an **application & an individual**. For more information, please refer to :ref:`How to secure an API <linking-pages-howto-secure>`

 
Finally, call the API and pass the access_token:

.. code-block:: python

   curl -vk -X GET --header 'Accept: application/json' --header 'Authorization: Bearer <YOUR_ACCES_TOKEN>' https://api-gateway-emea.integration.ca-cib.com/api_name/v1/endpoints
