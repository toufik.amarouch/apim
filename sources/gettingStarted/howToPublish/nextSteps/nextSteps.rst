.. _linking-pages-prod-publi:

##########
Next steps
##########

******************************
Prerequisites (Open APIs only)
******************************

^^^^

- Security requirements 

   - Before publishing your **Open** API on the API-Manager production store, you have to do penetration tests

      - Please contact the ISS team to initiate them at ISS_GCA_PAA@msx.cib.cal
      - The penetration tests report will be asked before any API publication on the production 

- Network & flow opening

   - You also have to open flows 
   - Please contact **IT4IT team** at support_apim@ca-cib.com ti initiate the demand

.. note:: As of today, the publishing portal is not open in selfcare for preproduction and production environments.

*************
Preproduction
*************

^^^^

To request the publishing of your API please contact **IT4IT team** at support_apim@ca-cib.com


.. note:: You will be asked for preproduction backend URL and the link to the version of your API (in the API-Manager store) you want to publish.

**********
Production
**********

^^^^

Base on your test and project milestone, plan the production publishing by contacting **IT4IT team** at support_apim@ca-cib.com

.. note:: You will be asked for production backend URL and the link to the version of your API (in the API-Manager store) you want to publish.

.. warning:: 
   
   - **Reminder**: Penetration tests have to be passed successfully. 
   - Note that you can request the publishing of your API before the actual Go Live of your backend API. In that manner, your API will be ready as soon as your backend will be deployed in production.

**********
CI/CD Automated API Publishing
**********

^^^^

CI/CD client API-Manager tool is not yet part of the service offer. However, if it is nedded, you can contact **IT4IT team** at support_apim@ca-cib.com