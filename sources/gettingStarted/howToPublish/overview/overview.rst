############
Overview
############

This section is dedicated to API providers.

.. figure::  provider_vf.png
   :align:   center

   API Providers


In this section, we will go through all steps to get backend application services exposed as API through the API-Manager. At the end, HTTP services provided by your application will be exposed through API-Manager as an API.

****************
Prerequisites
****************

^^^^

You will be asked for:

- A backend URL of the server exposing services (Example: https://your.application.domain/my_services)
- A swagger file describing specification of your services (Yaml or JSON file)

.. note:: Swagger specifications definition: :ref:`Appendix - Swagger specifications <linking-pages-appendix-swagger>`
