##################
Quick start guide
##################

Complete the below steps to publish your first API on the API-Manager.

.. warning:: This is a Quick start guide for publishing an API on a **non-production environment**. (example: DEV, INT, UAT, etc...)

.. seealso::

  - If you want to publish your API on the **preproduction environment**, please go to: :ref:`Publishing an API on the Preproduction Store <linking-pages-prod-publi>`
  - If you want to publish your API on the **production environment**, please go to: :ref:`Publishing an API on the Production Store <linking-pages-prod-publi>`

First, API providers should logged on API-Manager publisher web portal.

URLs access:

   - Open APIs: https://external-api.int.ca.cib/publisher
   - Private APIs: https://internal-api.int.ca.cib/publisher

.. note:: Publishing steps will be the same for both Open and Private APIs. However, please note that additional **security prerequisites** are mandatory for Open APIs (:ref:`Prerequisites for Open APIs <linking-pages-prod-publi>`)

***********************************************
Import your swagger specification file
***********************************************

^^^^

From home page:

- Click on "Add a new API"
- Then, click on "I Have an Existing API"
- Then, upload your Swagger by using either "Swagger File" or "Swagger URL" option
- Then, click on "Start Creating!"

You will be redirected to the creation pages. The creation is divided in 3 main steps: Design > Implement > Manage

.. tip:: All the steps and the fields describe below are **mandatory** to publish your API.

****************
Step 1 - Design
****************

^^^^

This section allow you to provide general information about your API.

Fill in the general details:

- **Name:** (mandatory and can not be updated later)

   - The name of the API displayed in the WSO2 Store
   - No space nor special characters

      - *Example: "My-Api-Name"*

- **Context:** (mandatory and can not be updated later)

   - Standardize the coding of the context zone that allows the API to be designated in the access URL

      - Standard: Use snake_case coding
      - *Example: "my_api_context"*

- **Version:**

   - The version appears in the API's consumption access URL, any version evolution will have an impact on the API call

      - Standard: major version (1 digit max) - environment
      - *Example: "v1-dev"*

.. note:: More information about versioning your API: :ref:`API Lifecycle standards <linking-pages-api-lifecycle>`


- **Description:**

   - Describe succinctly your API

***************************
Step 2 - Implement
***************************

^^^^

This section allow you to provide information about the implementation of the interface.

You will have the possibility to choose between "Managed API" and "Prototyped API".
Click on **"Managed API"**, and fill in the fields required:


- **Endpoint Type:**

   - You have to choose a type of endpoint from the list:

      - Standard: HTTP/REST Endpoint

   - "Load Balanced" case: Allow you to enable a load balancing policy by defining the endpoints that the service will connect to in case of a failure
   - "Failover" case stands for: Allow you to enable failover policy by defining the endpoints where  incoming requests will be directed to in case of a failure

      - Standard: "Load Balanced" and "Failover" cases stay **uncheck**

- **Endpoint:**

   - Production: Backend URL of the server exposing services
   - Sandbox: Mock endpoint that has the same functionality of the actual backend API

      - Standard: Just the **production endpoint** is needed

- **CORS configuration:**

   - Cross-Origin Resource Sharing (CORS) is a mechanism that allows accessing restricted resources (i.e., fonts, images, scripts, videos) from domains outside the domain from which the requesting resource originated

      - Standard: Enable this option

***********************************
Step 3 - Manage
***********************************

^^^^

This section allow you to define the technical settings.

In the "Configurations" section, fill in:

- **Transports:**

   - You have to choose between HTTP and HTTPS

      - Standard: Only HTTPS

In the "Throttling Settings" section, fill in:

- **Subscription Tiers:**

   - Allows you to specify the maximum number of requests allowed over a given time

      - Standard: By default "unlimited” but free to choose

- **API Properties:**

   - Allows you to add others properties to your API.

     - Standard: Please add two new "Additional Properties":
     - "property name" = **case_wise_application_code** and "property value" = **your actual application code** (example: MEDSVC)
     - "property name" =  **case_wise_application_link** and "property value" = **your actual application link** (example: http://ea.emea.cib/evolve/sites/ea/index.html#/cwtype=single&cwview=isapplication)


Save and publish!

************************************
Step 4 - Check and add documentation
************************************

^^^^

- Go to your **API Overview** and check all the information
- Then, in the "Docs" section, click on "Add New Document"
- Then, fill in a "Name" and a "Summary"
- Then, choose a "Type" and the "Source" of your documentation
- Finally, click on "Add Document"

Your API is published, it will be available in the API-Manager store web portal!

URLs access:

   - Open APIs: https://external-api.int.ca.cib/store
   - Private APIs: https://internal-api.int.ca.cib/store

