
#################
Glossary of terms
#################

- Private API

   - An API intended to be exposed to the Intranet, that means the API will be restricted to internal applications

- Open API

   - An API intended to be exposed to the Internet, that means the API will be available for external applications and therefore internal applications too

- CACIB intranet application 

   - An application from CACIB ecosystem that is exposed to Intranet  

      - *Example: Optim*

- CACIB internet application (in-house developement)

   - An application from CACIB ecosystem that is exposed to Internet 

      - *Example: DIANA*

- CACIB internet application (Cloud Saas)

   - An SaaS application from CACIB ecosystem that is exposed to Internet 

      - *Example: Salesforce*

- CACIB partner application

   - An application from the Cr�dit Agricole ecosystem

      - *Example: CA-SA application, CA-TS application*

- External application

   - An application outside of the Cr�dit Agricole ecosystem 

      - *Example: LVMH application*
