
#####################
API-Manager analytics
#####################

This section is dedicated to API providers & consumers. It will provide informations about the analytics you can get from the API-Manager.

The API-Manager provides another independent module called WSO2 Analytics. Based on traffic analysis, this module computes metrics (API usage, number of subscriptions) and generates alerts for both administrators and consumers / providers

.. note:: Admin portal: The API-Manager comes with a component called Admin portal. This portal allows the administrators to access to logs & fired alerts, and to edit settings (alert activation and thresholds)

Real time - Event management
****************************

^^^^

- **Event generation**: For each HTTP request, successful or not, handled by the gateway, a series of events is fired
- **Event listening**: It listens for all events emitted by the gateway and stores them in a dedicated database. At this point, based on the nature of the event captured, it can launch alerts for both publishers (abnormal response time, abnormal rate of errors) and consumers (Abnormal usage, requests throttled)

Batch - Computation and Aggregation & publishing
************************************************

^^^^

- **Computation and Aggregation**: Periodically, WSO2 Analytics executes routines to compute metrics (on usage, throttling, number of subscription) based on events captured at runtime
- **Viewing metrics**: All computed metrics are stored in dedicated tables described in the following slide. A minor part of them are available for developers from the developer portal and all of them are available from the publisher portal


For providers
*************

^^^^


- The publisher web portal provides an UI to view the computed metric and also an UI to manage alert settings
- Logs and fired alerts are available in the admin portal
- Fired alerts can also be received by email (subscription directly from publisher web portal)


For consumers
*************

^^^^

- Metrics and alert settings are available in the consumer / developer portal
- Fired alerts can also be received by email (subscription directly from store web portal)
