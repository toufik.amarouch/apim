.. _linking-pages-howto-secure:

########
Overview
########

**********************************
Security - Basics and definitions
**********************************

^^^^

This section explicites and introduces the differents terms used in API security. 

- **Authentication**: Process of validating the identity of a registered user who is accessing a service or application.

   - In an API context, 2 parties have to be authenticated: the consumer application and the end-user (if exists) 
  
      - Example: A website asks user for login/password to verify an identity


- **Authorization**: Process of making sure an authenticated application/user has the necessary privileges to access a specific resource

   - Brings the notions of roles (Customer, employee) and scopes (read_accounts, close_account)

      - Example: Unlike customers, employees are authorized to access a resource allowing to block an account


.. note:: Authentication and authorization is handled by an authorization server (using OAuth2/OpenID Connect)

- OAuth2: is a protocol (sometimes defined as a framework) used for the delegation of authorization 

- OpenID Connect (OIDC): is a simple identity layer (authentication) on top of the OAuth 2.0 protocol. It enables clients to verify the identity of the end-User based on the authentication performed by an Authorization Server (delegation of the authentication) 


**********************************
OAuth2 / OIDC objectives
**********************************

^^^^

- OAuth2/OIDC describes different strategies, called flows (or grants), to authenticate and authorize consumers in a secure way

- All OAuth2/OIDC flows share a same objective: 

   - Let the consumer applications get access to APIs, on behalf of an end-user or on their own, by providing a temporary access key
   - This key is called a token 

- The choice of one OAuth2/OIDC flow rather another one depends on who are the stakeholders involved in the Authentication/Authorization process 

   - Example: Is an end-user involved? Is the consumer application secure?

- The 4 different stakeholders are:

   - Resource owner (End-user): End-user who ��owns�� data stored by the API provider

      - Example: Individuals: employee, customer,...

   - Client (Relying party): The consumer application (internal or external) which has subscribed to API

      - Example: Bankin, CA mobile application,...

   - Resource server: API provider�s server which exposes end user�s data

      - It will be a CA-CIB application  

   - Authorization server: Server in charge of authentication/authorization and which delivers credentials/access tokens

      - It will be UsignOn