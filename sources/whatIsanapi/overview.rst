##################
Overview
##################


**********
Definition
**********

^^^^

API stands for Application Programming Interfaces. Since a few years, it usually refers to Web APIs and, in all parts of this documentation, we always implicitly refer to Web APIs.

**Web APIs** specifies how 2 applications interact in a simple and normalized fashion.

When talking about Web APIs, we usually distingue 2 parties:

- **Consumer**: A set of applications interacting with one or many APIs to leverage services provided by another application
- **Provider**: Application exposing some services through an API

In the Open world, Web APIs becomes the most popular way for applications to share services. One of the reason is that Web APIs rely on widely spread and digital friendly technologies:

- HTTP: The use of a such common protocol guarantees that almost all applications - no matter the technologies they used (Old/new, Programming language etc.) - are able to either consume or expose an API
- JSON: A light and friendly human readable representation to format data . 
 
   - Example of JSON:

.. code-block:: json

   {
    "id": " QIn7PLUS8Dl ",
    "value": 300,
    "currency": "USD"
   }

Beyond technologies, Web APIs respect a style of architecture maned REST (Representation State Transfer). REST pattern allows to leverage properties (scale, services portability) by respecting standards like:

- **Stateless**: No state, context or session is managed on server / API side. Each HTTP request contains all the necessary information to get it managed by the server. Among other things, it allows to leverage cloud architecture and the ability to share services between client applications (service portability)
- **Client / server separation**: Consumer and provider are 2 distinct layers / levels / applications. When interacting, the consumer is only aware of the API interface exposed by the provider. In this manner, there is no consideration on what are the technologies used by the consumer and the provider (Only HTTP/S) and no consideration on the nature of the consumer (Website, mobile, JavaScript app)
- **Cacheable**: Design in a such way that cache system can be leveraged almost immediately

Web APIs becomes very convenient for some types of application interactions:

- **Real-time**
- **Structured and light-weight data**
- **Pooling**: A consumer initiates an API call to leverage a service (By opposition to a push model)
- **Synchronous**: The consumer calls an API and get immediately a response


.. figure::  api_def_basic.png
   :align:   center

   Exemple of an API call
