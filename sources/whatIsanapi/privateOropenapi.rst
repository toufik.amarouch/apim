.. _linking-pages-open-vs-private:

####################
Private or Open API?
####################

^^^^

APIs exposed can be classified in 2 categories: **Open** & **Private**

   - Open API: An API intended to be exposed to the Internet, that means the API will be available for external applications and therefore internal applications too
   - Private API: An API intended to be exposed to the Intranet, that means the API will be restricted to internal applications 



.. figure::  openvsprivate_vf.png
   :align:   center


.. tip:: The different API consumption usecases (open or private) are listed in the :ref:`Environment mapping <linking-pages-env-mapping>` section



