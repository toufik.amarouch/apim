#########################
What is an elligible API?
#########################

^^^^

To be **able to publish your API on the API-Manager**, it has to follow the prerequisites below:

   - In scope: **Web API**, which means API based on widespread Internet standards (JSON and HTTP), and respecting REST architecture
   - API should follow the guidelines hosted in the **Enterprise Architecture Referential**, in order to ensure a consistent developer experience and a common approach to manage key transversal concerns (**API Design Standards** - http://git.ca.cib/collab/GlobalEA/DT/Documents/API%20Design%20Standards.pdf)


