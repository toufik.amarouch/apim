##################
Overview
##################

***********************
Definition
***********************

^^^^

The API-Manager comes as a new layer between the consumer and the provider. The purpose of the API-Manger is to expose all CACIB APIs internally but also externally, to CACIB clients, partners of CA Group and outside the group.

Besides the API-Manager allows you to manage the lifecycle ansd the versionning of your APIs. Provides you tools to monitor APIs and get analytics. And finally, it helps you to increase the security and the gorvernance of your APIs.


.. figure::  api_full_schema_vf.png
   :align:   center

   How does it work 

The **main features** are:

  - API Exposure with custom controls and SLA
  - API Discovery and subscription through a catalog
  - Analytics about API usage

The **positive impacts** are: 

  - Security
  - IS mastering
  - Operational efficiency

.. note:: More informations on **WSO2**: :ref:`API Manager service offer <linking-pages-APIM-serviceoffer>`


***********************
Components
***********************

^^^^

In CACIB context, WSO2 API-M � has been chosen as API Manager solution. From applicative perspective, CACIB API Manager is composed of 5 main components:

  - **Store:** Web portal for developer of client applications

  - **Publisher:** Web portal internal staff to manage exposure

  - **Gateway:** Act as a reverse proxy for all incoming API requests

  - **Traffic Manager:** Manage rate-limiting policies (throttling)

  - **Key Manager:** Control access through CA-CIB IAM (see note below)

.. note:: **CA-CIB IAM** is an application aiming at providing authentication and authorization services. In CACIB context, the application playing this role is Usignon (Maintained by ISS team). For more information follow: : :ref:`Related integrated applications <linking-pages-Related-app>`
